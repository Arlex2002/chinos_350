<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Ruta Prueba

Route::get('Hola',function(){
    echo "Hola";
});

Route::get('arreglo' , function(){

    //Definir Arreglo
    $Estudiantes=[ 
        "AN" => "Ana" ,
        "MA" => "Maria" ,
        "VA" =>"Valeria",
        "CA" => "Carlos"];
    /// Ciclo
    foreach($Estudiantes as $i => $E ){
        echo " $E tiene el indice de :$i </br> ";
    }


});

//Ruta De Paises 
Route::get('paises', function(){
    
    $paises= [
        "Colombia" =>[
                        "Capital" =>"Bogotá",
                        "Moneda" =>"Peso",
                        "Población" =>50372424,
                        "Ciudades"=>["Medellín", "Cali", "Santa Marta"]

                     ],
        "Peru"     =>[
                        "Capital" =>"Lima",
                        "Moneda" =>"Sol",
                        "Población" =>33050325,
                        "Ciudades"=>["Cuzco", "Arequipa", "Trujillo"]
                     ],
        "Ecuador"  =>[
                        "Capital" =>"Quito",
                        "Moneda" =>"Dólar",
                        "Población" =>17517141,
                        "Ciudades"=>["Guataquil", "Manta", "Tulcán"]
                     ],
        "Brasil"   =>[
                        "Capital" =>"Brasilia",
                        "Moneda" =>"Real",
                        "Población" =>212216052,
                        "Ciudades"=>["Rio de Janeiro", "Recife", "Sao Pablo"]
                     ] 
    ];
    //Enviar datos de paises a una vista
    //con la función view
    return view('paises')->with("paises", $paises);
});